import React from 'react';
import { Link } from 'react-router-dom';
import Header from "components/Header";
import Footer from "components/Footer";
const NotFound = () => (
    <>
        <Header />
        <div className="not-found">
            <img
                src="https://www.pngitem.com/pimgs/m/561-5616833_image-not-found-png-not-found-404-png.png"
                alt="not-found"
            />
            <Link to="/" className="link-home">
                Về trang chủ
            </Link>
        </div>
        <Footer />
    </>

);

export default NotFound;